<?php

namespace app\controllers;

use app\models\Albumes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AlbumesController implements the CRUD actions for Albumes model.
 */
class AlbumesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Albumes models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Albumes::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoAlbumes' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Albumes model.
     * @param int $codigoAlbumes Codigo Albumes
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoAlbumes)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoAlbumes),
        ]);
    }

    /**
     * Creates a new Albumes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Albumes();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoAlbumes' => $model->codigoAlbumes]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Albumes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigoAlbumes Codigo Albumes
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoAlbumes)
    {
        $model = $this->findModel($codigoAlbumes);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoAlbumes' => $model->codigoAlbumes]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Albumes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigoAlbumes Codigo Albumes
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoAlbumes)
    {
        $this->findModel($codigoAlbumes)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Albumes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigoAlbumes Codigo Albumes
     * @return Albumes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoAlbumes)
    {
        if (($model = Albumes::findOne(['codigoAlbumes' => $codigoAlbumes])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
