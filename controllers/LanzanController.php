<?php

namespace app\controllers;

use app\models\Lanzan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LanzanController implements the CRUD actions for Lanzan model.
 */
class LanzanController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Lanzan models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Lanzan::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoLanza' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Lanzan model.
     * @param int $codigoLanza Codigo Lanza
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoLanza)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoLanza),
        ]);
    }

    /**
     * Creates a new Lanzan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Lanzan();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoLanza' => $model->codigoLanza]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Lanzan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigoLanza Codigo Lanza
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoLanza)
    {
        $model = $this->findModel($codigoLanza);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoLanza' => $model->codigoLanza]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Lanzan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigoLanza Codigo Lanza
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoLanza)
    {
        $this->findModel($codigoLanza)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Lanzan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigoLanza Codigo Lanza
     * @return Lanzan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoLanza)
    {
        if (($model = Lanzan::findOne(['codigoLanza' => $codigoLanza])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
