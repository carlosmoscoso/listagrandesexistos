<?php

namespace app\controllers;

use app\models\Forman;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FormanController implements the CRUD actions for Forman model.
 */
class FormanController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Forman models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Forman::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoForman' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Forman model.
     * @param int $codigoForman Codigo Forman
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoForman)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoForman),
        ]);
    }

    /**
     * Creates a new Forman model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Forman();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoForman' => $model->codigoForman]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Forman model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigoForman Codigo Forman
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoForman)
    {
        $model = $this->findModel($codigoForman);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoForman' => $model->codigoForman]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Forman model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigoForman Codigo Forman
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoForman)
    {
        $this->findModel($codigoForman)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Forman model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigoForman Codigo Forman
     * @return Forman the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoForman)
    {
        if (($model = Forman::findOne(['codigoForman' => $codigoForman])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
