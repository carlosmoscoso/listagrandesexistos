<?php

namespace app\controllers;

use app\models\Artistas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ArtistasController implements the CRUD actions for Artistas model.
 */
class ArtistasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Artistas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Artistas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoArtistas' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Artistas model.
     * @param int $codigoArtistas Codigo Artistas
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoArtistas)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoArtistas),
        ]);
    }

    /**
     * Creates a new Artistas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Artistas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoArtistas' => $model->codigoArtistas]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Artistas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigoArtistas Codigo Artistas
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoArtistas)
    {
        $model = $this->findModel($codigoArtistas);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoArtistas' => $model->codigoArtistas]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Artistas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigoArtistas Codigo Artistas
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoArtistas)
    {
        $this->findModel($codigoArtistas)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Artistas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigoArtistas Codigo Artistas
     * @return Artistas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoArtistas)
    {
        if (($model = Artistas::findOne(['codigoArtistas' => $codigoArtistas])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
