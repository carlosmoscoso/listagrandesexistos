<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Albumes */

$this->title = $model->codigoAlbumes;
$this->params['breadcrumbs'][] = ['label' => 'Albumes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="albumes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigoAlbumes' => $model->codigoAlbumes], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigoAlbumes' => $model->codigoAlbumes], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigoAlbumes',
            'genero',
            'titulo',
        ],
    ]) ?>

</div>
