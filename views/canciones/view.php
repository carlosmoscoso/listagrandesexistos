<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Canciones */

$this->title = $model->codigoCanciones;
$this->params['breadcrumbs'][] = ['label' => 'Canciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="canciones-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigoCanciones' => $model->codigoCanciones], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigoCanciones' => $model->codigoCanciones], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigoCanciones',
            'nombre',
            'duracion',
            'f_lanzamiento',
            'reproducciones',
        ],
    ]) ?>

</div>
