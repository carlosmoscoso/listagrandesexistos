<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Forman */

$this->title = 'Create Forman';
$this->params['breadcrumbs'][] = ['label' => 'Formen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="forman-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
