<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Forman */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="forman-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigoCanciones')->textInput() ?>

    <?= $form->field($model, 'codigoAlbumes')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
