<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Forman */

$this->title = $model->codigoForman;
$this->params['breadcrumbs'][] = ['label' => 'Formen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="forman-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigoForman' => $model->codigoForman], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigoForman' => $model->codigoForman], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigoForman',
            'codigoCanciones',
            'codigoAlbumes',
        ],
    ]) ?>

</div>
