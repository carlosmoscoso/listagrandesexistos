<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Forman */

$this->title = 'Update Forman: ' . $model->codigoForman;
$this->params['breadcrumbs'][] = ['label' => 'Formen', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoForman, 'url' => ['view', 'codigoForman' => $model->codigoForman]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="forman-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
