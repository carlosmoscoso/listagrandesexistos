<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Lanzan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lanzan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigoArtistas')->textInput() ?>

    <?= $form->field($model, 'codigoCanciones')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
