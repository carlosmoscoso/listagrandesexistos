<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lanzan */

$this->title = 'Update Lanzan: ' . $model->codigoLanza;
$this->params['breadcrumbs'][] = ['label' => 'Lanzans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoLanza, 'url' => ['view', 'codigoLanza' => $model->codigoLanza]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lanzan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
