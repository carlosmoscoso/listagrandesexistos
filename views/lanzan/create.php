<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lanzan */

$this->title = 'Create Lanzan';
$this->params['breadcrumbs'][] = ['label' => 'Lanzans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lanzan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
