<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Lanzan */

$this->title = $model->codigoLanza;
$this->params['breadcrumbs'][] = ['label' => 'Lanzans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="lanzan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigoLanza' => $model->codigoLanza], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigoLanza' => $model->codigoLanza], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigoLanza',
            'codigoArtistas',
            'codigoCanciones',
        ],
    ]) ?>

</div>
