<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lanzan".
 *
 * @property int $codigoLanza
 * @property int $codigoArtistas
 * @property int $codigoCanciones
 *
 * @property Artistas $codigoArtistas0
 * @property Canciones $codigoCanciones0
 */
class Lanzan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lanzan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoArtistas', 'codigoCanciones'], 'required'],
            [['codigoArtistas', 'codigoCanciones'], 'integer'],
            [['codigoArtistas'], 'exist', 'skipOnError' => true, 'targetClass' => Artistas::className(), 'targetAttribute' => ['codigoArtistas' => 'codigoArtistas']],
            [['codigoCanciones'], 'exist', 'skipOnError' => true, 'targetClass' => Canciones::className(), 'targetAttribute' => ['codigoCanciones' => 'codigoCanciones']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoLanza' => 'Codigo Lanza',
            'codigoArtistas' => 'Codigo Artistas',
            'codigoCanciones' => 'Codigo Canciones',
        ];
    }

    /**
     * Gets query for [[CodigoArtistas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoArtistas0()
    {
        return $this->hasOne(Artistas::className(), ['codigoArtistas' => 'codigoArtistas']);
    }

    /**
     * Gets query for [[CodigoCanciones0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCanciones0()
    {
        return $this->hasOne(Canciones::className(), ['codigoCanciones' => 'codigoCanciones']);
    }
}
