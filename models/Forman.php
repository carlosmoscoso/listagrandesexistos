<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "forman".
 *
 * @property int $codigoForman
 * @property int $codigoCanciones
 * @property int $codigoAlbumes
 *
 * @property Albumes $codigoAlbumes0
 * @property Canciones $codigoCanciones0
 */
class Forman extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'forman';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoCanciones', 'codigoAlbumes'], 'required'],
            [['codigoCanciones', 'codigoAlbumes'], 'integer'],
            [['codigoAlbumes'], 'exist', 'skipOnError' => true, 'targetClass' => Albumes::className(), 'targetAttribute' => ['codigoAlbumes' => 'codigoAlbumes']],
            [['codigoCanciones'], 'exist', 'skipOnError' => true, 'targetClass' => Canciones::className(), 'targetAttribute' => ['codigoCanciones' => 'codigoCanciones']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoForman' => 'Codigo Forman',
            'codigoCanciones' => 'Codigo Canciones',
            'codigoAlbumes' => 'Codigo Albumes',
        ];
    }

    /**
     * Gets query for [[CodigoAlbumes0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAlbumes0()
    {
        return $this->hasOne(Albumes::className(), ['codigoAlbumes' => 'codigoAlbumes']);
    }

    /**
     * Gets query for [[CodigoCanciones0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCanciones0()
    {
        return $this->hasOne(Canciones::className(), ['codigoCanciones' => 'codigoCanciones']);
    }
}
