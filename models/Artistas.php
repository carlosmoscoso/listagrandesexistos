<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "artistas".
 *
 * @property int $codigoArtistas
 * @property string $nombre
 * @property int|null $codigoGrupos
 *
 * @property Grupos $codigoGrupos0
 * @property Lanzan[] $lanzans
 */
class Artistas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'artistas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['codigoGrupos'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
            [['codigoGrupos'], 'exist', 'skipOnError' => true, 'targetClass' => Grupos::className(), 'targetAttribute' => ['codigoGrupos' => 'codigoGrupos']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoArtistas' => 'Codigo Artistas',
            'nombre' => 'Nombre',
            'codigoGrupos' => 'Codigo Grupos',
        ];
    }

    /**
     * Gets query for [[CodigoGrupos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoGrupos0()
    {
        return $this->hasOne(Grupos::className(), ['codigoGrupos' => 'codigoGrupos']);
    }

    /**
     * Gets query for [[Lanzans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLanzans()
    {
        return $this->hasMany(Lanzan::className(), ['codigoArtistas' => 'codigoArtistas']);
    }
}
