<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "canciones".
 *
 * @property int $codigoCanciones
 * @property string $nombre
 * @property int $duracion
 * @property string $f_lanzamiento
 * @property int $reproducciones
 *
 * @property Forman[] $formen
 * @property Lanzan[] $lanzans
 */
class Canciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'canciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'duracion', 'f_lanzamiento', 'reproducciones'], 'required'],
            [['duracion', 'reproducciones'], 'integer'],
            [['f_lanzamiento'], 'safe'],
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoCanciones' => 'Codigo Canciones',
            'nombre' => 'Nombre',
            'duracion' => 'Duracion',
            'f_lanzamiento' => 'F Lanzamiento',
            'reproducciones' => 'Reproducciones',
        ];
    }

    /**
     * Gets query for [[Formen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFormen()
    {
        return $this->hasMany(Forman::className(), ['codigoCanciones' => 'codigoCanciones']);
    }

    /**
     * Gets query for [[Lanzans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLanzans()
    {
        return $this->hasMany(Lanzan::className(), ['codigoCanciones' => 'codigoCanciones']);
    }
}
