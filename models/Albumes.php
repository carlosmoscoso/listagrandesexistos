<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "albumes".
 *
 * @property int $codigoAlbumes
 * @property string $genero
 * @property string $titulo
 *
 * @property Forman[] $formen
 */
class Albumes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'albumes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['genero', 'titulo'], 'required'],
            [['genero', 'titulo'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoAlbumes' => 'Codigo Albumes',
            'genero' => 'Genero',
            'titulo' => 'Titulo',
        ];
    }

    /**
     * Gets query for [[Formen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFormen()
    {
        return $this->hasMany(Forman::className(), ['codigoAlbumes' => 'codigoAlbumes']);
    }
}
